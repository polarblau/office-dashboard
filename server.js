'use strict';

const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConfig = require('./webpack.config.js');

const config = require('./config.js');
const WeatherService = require('./lib/services/weather');
const BVGService = require('./lib/services/bvg');
const NewsService = require('./lib/services/news');

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 3000 : process.env.PORT;
const app = express();

app.use(express.static(__dirname + '/dist'));

if (isDeveloping) {
  const compiler = webpack(webpackConfig);
  const middleware = webpackMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath,
    contentBase: 'src',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));

  app.get('/', function response(req, res) {
    res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
    res.end();
  });
} else {
  app.get('/', function response(req, res) {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });
}

// ----------------------------------------------------------------------------

const bvgService = new BVGService(config.bvg);

app.get('/api/bvg/stations', (req, res) => {
  bvgService
    .requestStations(req.query.address)
    .then(data => res.json(data))
    .catch(error => res.status(500).json({ error }));
});

app.get('/api/bvg/departures', (req, res) => {
  bvgService
    .requestDepartures(req.query.stationID)
    .then(data => res.json(data))
    .catch(error => res.status(500).json({ error }));
});

// ----------------------------------------------------------------------------

const weatherService = new WeatherService(config.weather);

app.get('/api/weather', (req, res) => {
  weatherService
    .refresh()
    .then(data => res.json(data))
    .catch(error => res.status(500).json({ error }));
});

// ----------------------------------------------------------------------------

const newsService = new NewsService(config.news);

app.get('/api/news', (req, res) => {
  newsService
    .refresh()
    .then(data => res.json(data))
    .catch(error => res.status(500).json({ error }));
});

// ----------------------------------------------------------------------------

app.listen(port, '0.0.0.0', function onStart(err) {
  if (err) {
    console.log(err);
  }
  console.info('==> 🌎 Listening on port %s.', port);
});
