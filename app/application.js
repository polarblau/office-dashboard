import './styles/application.sass';

// ----------------------------------------------------------------------------

import React from 'react';
import ReactDOM from 'react-dom';

import Dashboard from './components/dashboard.jsx';

// ----------------------------------------------------------------------------

ReactDOM.render(<Dashboard />, document.getElementById('dashboard'));
