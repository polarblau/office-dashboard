import SortedQueue from './sorted_queue.js';
import SortedQueueMember from './sorted_queue_member.js';

const DEFAULTS = {
  delay: 0,
  walkingDuration: 0,
  graceDuration: 1000
}

class Departure extends SortedQueueMember {

  constructor(data, options={ graceDuration: 1000 * 20 }) {
    super(data, DEFAULTS);
    this.options = options;
    this._sanityCheck();
  }

  isExpired() {
    return this.data.timeLeft <= 0 || (!this._isReachable() && !this._isWithinGracePeriod());
  }

  //

  _isReachable() {
    return this.data.walkingDuration < this.data.timeLeft;
  }

  _isWithinGracePeriod() {
    return this.data.walkingDuration - this.data.timeLeft < this.options.graceDuration;
  }

  _caculateTimeLeft() {
    this.data.timeLeft = Math.max(0, this.data.date - Date.now());
    // this.data.timeLeft = Math.max(0, this.data.date - (this.data.delay * 1000) - Date.now());
  }

  _evaluate() {
    this._caculateTimeLeft();
    super._evaluate();
  }

  _sanityCheck() {
    this._caculateTimeLeft();
    if (this.data.delay === undefined ) this.data.delay = 0;
  }

}

export default class DepartureQueue extends SortedQueue {
  constructor(items=[], options) {
    super(items, options);
  }
}

DepartureQueue.memberClass = Departure;
