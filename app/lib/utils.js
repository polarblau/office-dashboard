const Utils = {

  padLeft(string, width, pad) {
    pad = pad || '0';
    string = string + '';
    return string.length >= width ?
      string :
      new Array(width - string.length + 1).join(pad) + string;
  },

  toClassName(string) {
    return string.trim().replace(/[-_\s]+/g, '-').toLowerCase();
  },

  formatParams(params) {
    const pairs = [];
    for (var key in params) {
      if (!params.hasOwnProperty(key)) continue;

      if (Object.prototype.toString.call(params[key]) == "[object Object]") {
        pairs.push(Utils.formatParams(params[key]));
        continue;
      }

      pairs.push(key + "=" + params[key]);
    }
    return pairs.join("&");
  },

  formatTimeLeft(ms) {
    const min = (ms / 1000 / 60) << 0;
    const sec = (ms / 1000) % 60;

    return `${Utils.padLeft(min.toFixed(0), 2)}:${Utils.padLeft(sec.toFixed(0), 2)}`;
  },

  formatTemperature(temperature, unit='C') {
    let value = temperature.toFixed(0);

    switch(unit) {
      case 'F':
        return `${value}°F`;
        break;
      case 'C':
      default:
        return `${value}°C`;
    }
  },

  getWeekDay(date) {
    return [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday'
    ][date.getDay()];

  },

  scale(d, r) {
    return (value) => (((value - d[0]) * (r[1] - r[0])) / (d[1] - d[0])) + r[0];
  }

}

export default Utils;
