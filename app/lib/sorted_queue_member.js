export default class SortedQueueMember {

  constructor(data, defaults= {}) {
    this.data = Object.assign({}, defaults, data);
  }

  get(key) {
    return this.data[key];
  }

  isExpired() {
    return this.date > Date.now();
  }

  toData() {
    return this.data;
  }

  //

  _evaluate() {}

}
