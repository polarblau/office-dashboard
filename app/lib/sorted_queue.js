import Eventable from './eventable.js';
import SortedQueueMember from './sorted_queue_member.js';

const DEFAULT_OPTIONS = { idKey: 'id', sortKey: 'date' };

export default class SortedQueue extends Eventable {

  constructor(items=[], options={}) {
    super();

    this.items   = [];
    this.options = Object.assign({}, DEFAULT_OPTIONS, options);

    items.forEach((item) => this.push(item));
  }

  push(item) {
    if (Array.isArray(item)) {
      item.forEach((i) => this.push(i));
      return;
    }

    if (this.find(item[this.options.idKey])) {
      return;
    }

    this.items.push(new this.constructor.memberClass(item));
    this.sort();
    this._evaluate();
  }

  find(id) {
    return this.items.some((item) => item.get(this.options.idKey) === id);
  }

  toData() {
    this._evaluate();
    return this.items.map((item) => item.toData());
  }

  sort() {
    this.items.sort((a, b) => a.get(this.options.sortKey) > b.get(this.options.sortKey));
  }

  //

  _evaluate() {
    // TODO: filter?
    let items = [];
    this.items.forEach((item) => {
      if (!item.isExpired()) {
        item._evaluate();
        items.push(item);
      }
    });
    this.items = items;
  }

}

SortedQueue.memberClass = SortedQueueMember;
