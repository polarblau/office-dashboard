export default class Eventable {

  constructor() {
    this.listeners = new Map();
  }

  on(event, callback) {
    this.listeners.has(event) || this.listeners.set(event, []);
    this.listeners.get(event).push(callback);
  }

  off(event, callback) {
    let listeners = this.listeners.get(event);

    if (!listeners || !listeners.length) return false;

    if (callback) {
      this.listeners.set(event, listeners.filter((listener) =>
        typeof listener == 'function' && listener !== callback
      ));
    } else {
      this.listeners.set(event, []);
    }
    return true;
  }

  trigger(event, ...args) {
    let listeners = this.listeners.get(event);

    if (listeners && listeners.length) {
      listeners.forEach((listener) => listener(...args));
      return true;
    }
    return false;
  }

}