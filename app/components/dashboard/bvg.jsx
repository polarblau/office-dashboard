import retryPromise from 'promise-retry';

import React from 'react';
import Utils from '../../lib/utils.js';
import Station from './bvg/station.jsx';


// ----------------------------------------------------------------------------

const END_POINT      = '/api/bvg/stations';
const WAITING_BUFFER = 1.2; // % of total waiting period

// ----------------------------------------------------------------------------


export default class Bvg extends React.Component {

  constructor(props) {
    super(props);

    this.retryCount = 1;
    this.state = {
      stations: null,
      waitingTimes: {},
      loadingState: 'pending',
      queryAddress: props.config.address,
      timeScale: Utils.scale([0, 100], [0, 100])
    };
  }

  componentDidMount() {
    retryPromise(retry => this.fetchStations(this.state.queryAddress)
      .catch(retry)).then(
        () => this.setState({ loadingState: 'succeeded' }),
        () => this.setState({ loadingState: 'failed' })
      );
  }

  fetchStations(address) {
    // TODO: max stations as query param?
    return fetch(`${END_POINT}?address=${encodeURIComponent(address)}`)
      .then(response => {
        if (!response.ok) throw Error(response.statusText);
        return response;
      })
      .then(response => response.json())
      .then(response => this.setState({ stations: response.stations }));
  }

  updateTimeScale(stationMaxWaitingTime) {
    let waitingTimes = Object.assign(this.state.waitingTimes, stationMaxWaitingTime);
    let waitingTimesValues = Object.values(this.state.waitingTimes);
    let maxWaitingTime = Math.max(...[].concat.apply([], waitingTimesValues));

    this.setState({
      waitingTimes,
      timeScale: Utils.scale([0, maxWaitingTime * WAITING_BUFFER], [0, 100])
    });
  }

  render() {
    let stations = this.state.stations;

    if (stations) {
      stations = this.state.stations.map(station => {
        return <Station {...station} timeScale={this.state.timeScale}
                                     dispatchMaxWaitingTime={this.updateTimeScale.bind(this)}
                                     key={station.id} />;
      });
    }

    let cssClasses = ['bvg', this.state.loadingState].join(' ');

    return (
      <div className={cssClasses}>
        {stations === null ? 'Loading ...' : stations}
      </div>
    );
  }

}
