import React from 'react';

import Utils from '../../lib/utils.js';
import SortedQueue from '../../lib/sorted_queue.js';

// ----------------------------------------------------------------------------

const END_POINT        = "/api/weather";
const FETCH_INTERVAL   = 60 * 1000; // 1min

// ----------------------------------------------------------------------------

export default class Weather extends React.Component {

  constructor(props) {
    super(props);

    this.state = { today: null, week: [], error: null };
    this.queue = new SortedQueue();
  }

  componentDidMount() {
    this.fetch();
    this.fetchTick = setInterval(this.fetch.bind(this), FETCH_INTERVAL);
  }

  componentWillUnmount() {
    clearInterval(this.fetchTick);
  }

  fetch() {
    this._requestWeather((data) => {
      if (data.error) {
        this.setState({ data });
      } else {
        this.queue.push(data.week);
        this.setState({ today: data.today, week: this.queue.toData(), error: null });
      }
    });
  }


  render() {
    if (this.state.error) {
      return <div className='error'>{this.state.error}</div>
    }

    if (!this.state.today || this.state.week.length <= 0) {
      return <div className='loading'>Loading...</div>
    }

    let today = (<div>
      {Utils.formatTemperature(this.state.today.temp)}, {this.state.today.description}
      <br/>
      Min: {Utils.formatTemperature(this.state.today.minTemp)} / Max: {Utils.formatTemperature(this.state.today.maxTemp)}
    </div>);

    /*
    let week = this.state.week.map(day => {
      return (<li key={day.id}>
        <img src={`images/components/weather/icons/${day.icon}.svg`} alt={day.weather} />
        { Utils.getWeekDay(new Date(day.date)) }
        { Math.round(day.maxTemp) }°C/
        { Math.round(day.minTemp) }°C
        <br />
        { day.humidity }%
        <br />
        { day.windSpeed.toFixed(1) }m/s
      </li>);
    });
    */

    return (
      <div className="weather">
        <div className="today">
          {this.state.today && today}
        </div>
      </div>
    );
  }

  // --------------------------------------------------------------------------

  _requestWeather(handleSuccess) {
    fetch(END_POINT)
      .then(response => response.json())
      .then(handleSuccess)
      .catch(() => console.error("Weather#_requestWeatherDays:", ...arguments));
  }

}
