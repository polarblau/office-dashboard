import React from 'react';

// ----------------------------------------------------------------------------

export default class AddressForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = { address: null };

    this.handleAddressSubmit = this.handleAddressSubmit.bind(this);
    this.handleAddressChange = this.handleAddressChange.bind(this);
  }

  handleAddressChange(event) {
    this.setState({ address: event.target.value });
  }

  handleAddressSubmit(event) {
    event.preventDefault();
    if (!this.state.address) return;

    this.props.handleAddressUpdate(this.state.address);
    this.setState({ address: null });
  }

  render() {
    return (
      <form className="address" onSubmit={this.handleAddressSubmit}>
        <label>
          Address:
          <input type="text" onChange={this.handleAddressChange} placeholder="Search for an address ..." />
        </label>
        <button type="submit">Fetch!</button>
      </form>
    );
  }
}
