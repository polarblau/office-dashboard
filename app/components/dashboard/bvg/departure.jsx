import React from 'react';
import Utils from '../../../lib/utils.js';

// ----------------------------------------------------------------------------

const Departure = (props) => {
  const reachableClass = props.isReachable ? 'reachable' : 'not-reachable';
  const delayedClass = +props.delay !== 0 ? props.delay < 0 ? 'delayed early' : 'delayed late' : 'on-time';

  return (
    <div className={['departure', reachableClass, delayedClass].join(' ')} title={new Date(props.date)}>

      <div className={`line ${Utils.toClassName(props.line)} ${props.product}`}>
        {props.line}
      </div>

      <div className="progress">
        <div className="position" style={{ right: `${props.timeScale(props.timeLeft)}%`, width: `${Math.abs(props.timeScale(props.delay * 1000))}%` }}>
          <div className="label">
            <span>{Utils.formatTimeLeft(props.timeLeft)}</span>
            <span className="delay">{props.delay > 0 && '+'}{props.delay}</span>
          </div>
          <div className="delta" ></div>
          <div className="actual"></div>
          <div className="scheduled"></div>
        </div>
        <div className="track"></div>
        <div className="walking-duration" style={{ width: `${props.timeScale(props.walkingDuration)}%`}}></div>
      </div>

      <div className="direction">
        {props.direction}
      </div>

    </div>
  );
};

export default Departure;
