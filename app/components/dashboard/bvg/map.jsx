import React from 'react';
import { withGoogleMap, GoogleMap, Marker, Circle } from 'react-google-maps';

import mapStyles from "./map_styles.json";

// ----------------------------------------------------------------------------

const GoogleMapsMap = withGoogleMap(props => (
  <GoogleMap
    defaultZoom={15}
    defaultCenter={props.center}
    defaultOptions={{ styles: mapStyles, disableDefaultUI: true }}>
    <Marker
      position={props.center}
      icon={{url: "images/components/bvg/marker_icon_dka.png", scaledSize: new google.maps.Size(50, 58) }}
    />
    {props.stations.map((station, i) => (
      <Marker
        key={i}
        position={{
          lat: station.coordinates.latitude,
          lng: station.coordinates.longitude
        }}
        icon={{url: "images/components/bvg/marker_icon_stop.png", scaledSize: new google.maps.Size(20, 20), anchor: new google.maps.Point(10, 10) }}
      />
    ))}
  </GoogleMap>
));

export default class Map extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <GoogleMapsMap
        stations={this.props.stations}
        center={{
          lat: this.props.origin.latitude,
          lng: this.props.origin.longitude
        }}
        containerElement={
          <div className="map" />
        }
        mapElement={
          <div style={{ height: `100%` }} />
        }
      />
    );
  }
}
