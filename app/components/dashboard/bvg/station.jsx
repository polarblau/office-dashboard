import retryPromise from 'promise-retry';

import React from 'react';
import Utils from '../../../lib/utils.js';

import DepartureList from './departure_list.jsx';

// ----------------------------------------------------------------------------

const END_POINT      = '/api/bvg/departures';
const FETCH_INTERVAL   = 10 * 1000; // 10sec

// ----------------------------------------------------------------------------

const calculateMaxWaitingTime = (departures) => {
  let departureDates = departures.map(departure => departure.date);
  let durations = departureDates.map(date => Date.parse(date) - Date.now());
  let maxWaitingTime = Math.max(...[].concat.apply([], durations));

  return maxWaitingTime;
}

// ----------------------------------------------------------------------------

export default class Station extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loadingState: 'pending',
      departures: null
    }
  }

  componentDidMount() {
    retryPromise(retry => this.fetchDepartures(this.props.id)
      .catch(retry)).then(
        () => {
          this.fetchTick = setInterval(this.fetchDepartures.bind(this), FETCH_INTERVAL, this.props.id);
        }
      );
  }

  componentWillUnmount() {
    clearInterval(this.fetchTick);
  }

  fetchDepartures(stationID) {
    this.setState({ loadingState: 'pending' });

    return fetch(`${END_POINT}?stationID=${encodeURIComponent(stationID)}`)
      .then(response => {
        if (!response.ok) throw Error(response.statusText);
        return response;
      })
      .then(response => response.json())
      .then(departures => {
        this.props.dispatchMaxWaitingTime({ [this.props.id]: calculateMaxWaitingTime(departures)});
        return departures;
      })
      .then(departures => this.setState({ departures, loadingState: 'succeeded' }))
      .catch(e => {
        this.setState({ loadingState: 'failed' });
        throw e;
      })
  }

  render() {
    const addProps = {
      timeScale: this.props.timeScale,
      walkingDuration: this.props.duration * 1000
    };

    let departures=  'Loading …';

    if (this.state.departures === null) {
    } else if (this.state.departures.length > 0) {
      departures = <DepartureList departures={this.state.departures} {...addProps} />
    } else if (this.state.departures.length === 0) {
      departures = 'No departures.';
    }

    let cssClasses = ['station', this.state.loadingState].join(' ');


    return (
      <div className={cssClasses}>
        <h2>
          {this.props.name}
          <small>{this.props.distance}m / {Utils.formatTimeLeft(this.props.duration * 1000)}min</small>
        </h2>
        { departures }
      </div>
    );
  };

}
