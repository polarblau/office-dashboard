import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import Departure from './departure.jsx';

import DepartureQueue from '../../../lib/departure_queue.js';

// ----------------------------------------------------------------------------

const RENDER_INTERVAL  = 500;

// ----------------------------------------------------------------------------

const formatDeparture = (departure, walkingDuration) => {
  const departureClone = Object.assign({}, departure, { walkingDuration });
  departureClone.date = Date.parse(departureClone.date);
  departureClone.line = departureClone.line.replace(/^Tram\ /, "");
  return departureClone;
};

// ----------------------------------------------------------------------------

export default class DepartureList extends React.Component {

  constructor(props) {
    super(props);
    const departures = props.departures.map(departure => {
      return formatDeparture(departure, props.walkingDuration);
    });

    this.queue = new DepartureQueue(departures, { graceDuration: 30 });
    this.state = { departures: this.queue.toData() };
  }

  componentDidMount() {
    this.renderTick = setInterval(this.update.bind(this), RENDER_INTERVAL);
  }

  componentWillReceiveProps(props) {
    this.queue.push(props.departures.map(departure => {
      return formatDeparture(departure, props.walkingDuration);
    }));
    this.update();
  }

  componentWillUnmount() {
    clearInterval(this.renderTick);
  }

  update() {
    this.setState({ departures: this.queue.toData() });
  }

  render() {
    let departures = this.state.departures.map(departure => {
      const addProps = {
        timeScale: this.props.timeScale,
        walkingDuration: this.props.walkingDuration,
        isReachable: this.props.walkingDuration < departure.timeLeft,
        key: departure.id
      };
      return <Departure {...departure} {...addProps} />;
    });

    if (departures.length == 0) departures = <div>No departures.</div>;

    return (
      <div className="departure-list">
        <ReactCSSTransitionGroup component="div"
                                 className="departures"
                                 transitionName="departure"
                                 transitionEnterTimeout={500}
                                 transitionLeaveTimeout={300}>
          {departures}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
};
