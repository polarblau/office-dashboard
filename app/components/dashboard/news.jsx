import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import Utils from '../../lib/utils.js';
import SortedQueue from '../../lib/sorted_queue.js';

// ----------------------------------------------------------------------------

const END_POINT        = "/api/news";
const FETCH_INTERVAL   = 60 * 1000; // every minute
const MAX_COUNT        = 5;

// ----------------------------------------------------------------------------

export default class News extends React.Component {

  constructor(props) {
    super(props);

    this.state = { items: [] };
    this.queue = new SortedQueue();
  }

  componentDidMount() {
    this.fetch();
    this.fetchTick  = setInterval(this.fetch.bind(this), FETCH_INTERVAL);
  }

  componentWillUnmount() {
    clearInterval(this.fetchTick);
  }

  fetch() {
    this._requestNews(items => {
      this.queue.push(items);
      this.update();
    });
  }

  update() {
    this.setState({ items: this.queue.toData() });
  }

  render() {
    let items = this.state.items.slice(0, MAX_COUNT).map(item => {
      return (<li key={item.id}>
        <h3>{item.title}</h3>
        <p>{item.description}</p>
        <p className="meta">
          <span className="date">{item.date}</span>,
          <span className="source">{item.source}</span>
        </p>
      </li>);
    });

    return (
      <div>
        <h2>News</h2>
        <ReactCSSTransitionGroup component="ul"
                                 className="items"
                                 transitionName="item"
                                 transitionEnterTimeout={500}
                                 transitionLeaveTimeout={300}>
          {items}
        </ReactCSSTransitionGroup>
      </div>
    );
  }

  // --------------------------------------------------------------------------

  _requestNews(success) {
    fetch(END_POINT)
      .then(response => response.json())
      .then(success)
      .catch(function() {
        console.error("News#_requestNews:", ...arguments);
      });
  }

}
