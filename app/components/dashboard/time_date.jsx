import React from 'react';
import Utils from '../../lib/utils.js';

// ----------------------------------------------------------------------------

const INTERVAL = 1000;

// ----------------------------------------------------------------------------

export default class TimeDate extends React.Component {

  constructor(props) {
    super(props);
    this.state = this._getDateTime();
  }

  componentDidMount() {
    this.tick = setInterval(this.update.bind(this), INTERVAL)
  }

  componentWillUnmount() {
    clearInterval(this.tick);
  }

  update() {
    this.setState(this._getDateTime());
  }

  render() {
    return (
      <div>
        {this.state.hours}:{this.state.minutes}
        <br />
        {this.state.dayOfWeek}, {this.state.day}.{this.state.month}.
      </div>
    );
  }

  //

  _getDateTime() {
    let now       = new Date();
    let hours     = Utils.padLeft(now.getHours(), 2);
    let minutes   = Utils.padLeft(now.getMinutes(), 2);
    let dayOfWeek = Utils.getWeekDay(now);
    let day       = Utils.padLeft(now.getDate(), 2);
    let month     = Utils.padLeft(now.getMonth() + 1, 2);

    return { hours, minutes, dayOfWeek, day, month };
  }
}