import Config from '../../config.js';
import React from 'react';

import TimeDate from './dashboard/time_date.jsx';
import Bvg from './dashboard/bvg.jsx';
import Weather from './dashboard/weather.jsx';

export default class Dashboard extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <TimeDate config={Config.timeDate} />
        <Weather config={Config.weather} />
        <Bvg config={Config.bvg} />
      </div>
    );
  }
}
