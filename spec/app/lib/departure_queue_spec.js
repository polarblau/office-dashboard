import assert from 'assert';
import timekeeper from 'timekeeper';
import DepartureQueue from '../../../app/lib/departure_queue';

const now = Date.parse('2017-06-10T17:30:00.000Z');
const then = now + 1000 * 60 * 60 * 12; // 12h from now
const item = { id: 1, date: +then, delay: 0, walkingDuration: 1000 * 60, graceDuration: 1000 * 60};

const getIds = (data) => data.map(d => d.id);

const formatDeparture = (departure) => {
  const departureClone = Object.assign({}, departure);
  departureClone.date = Date.parse(departureClone.date);
  return departureClone;
};

describe('DepartureQueue', function() {
  beforeEach(function() {
    timekeeper.freeze(now);
  });

  afterEach(function() {
    timekeeper.reset();
  });

  describe('#push()', function() {
    it('should be empty by default', function() {
      const q = new DepartureQueue();
      assert.equal(q.toData().length, 0);
    });

    it('should add items when pushed', function() {

      const q = new DepartureQueue();
      q.push(item);
      assert.equal(q.toData().length, 1);
    });

    it('should register a pushed item', function() {
      const q = new DepartureQueue();
      q.push(item);
      assert.deepEqual(getIds(q.toData()), [item.id]);
    });

    it('should allow pushing a second element', function() {
      const q = new DepartureQueue();
      const otherItem = Object.assign({}, item, { id: 2 });
      q.push(item);
      q.push(otherItem);
      assert.deepEqual(getIds(q.toData()), [item.id, otherItem.id]);
    });

    it('should not allow pushing an already existing element', function() {
      const q = new DepartureQueue();
      q.push(item);
      q.push(item);
      assert.deepEqual(getIds(q.toData()), [item.id]);
    });

    it('should sort elements according to date', function() {
      const q = new DepartureQueue();
      const youngerItem = Object.assign({}, item, { id: 2, date: item.date - 1000 * 60 });
      q.push(item);
      q.push(youngerItem);
      assert.deepEqual(getIds(q.toData()), [youngerItem.id, item.id]);
    });
  });

  describe('#_caculateTimeLeft()', function() {
    it('should set the "timeLeft" key', function() {
      const q = new DepartureQueue();
      q.push(item);
      assert.equal(item['timeLeft'], undefined);
      assert.notEqual(q.toData()[0]['timeLeft'], undefined);
    });

    it('should calculate the correct time', function() {
      const q = new DepartureQueue();
      q.push(item);
      const twelveHours = 1000 * 60 * 60 * 12;
      assert.equal(q.toData()[0]['timeLeft'], twelveHours);
    });
  });

  describe.skip('real case', function() {
    it('should work', function() {
      const q = new DepartureQueue();
      q.push([
        {
          'id': '939ec041520a83af17cc96b35646ac6fad49a4db',
          'name': '20 min ago, 3min delay',
          'date': '2017-06-10T17:10:00.000Z',
          'walkingDuration': 1000 * 60,
          'delay': -180
        },
        {
          'id': 'a2c86cccc78e1f722251576a58224d2cb7298b5e',
          'name': 'now but 8 min late',
          'date': '2017-06-10T17:30:00.000Z',
          'walkingDuration': 1000 * 60,
          'delay': -480
        },
        {
          'id': 'a59195fb6c4827e0921ce5dc87267eb7d2e80452',
          'name': 'in 4 min',
          'date': '2017-06-10T17:34:00.000Z',
          'walkingDuration': 1000 * 60,
          'delay': 0
        },
        {
          'id': '2b45dd65a3961fcb73b1fda4f35775cc27a3dfa8',
          'name': 'in 9 min',
          'date': '2017-06-10T17:39:00.000Z',
          'walkingDuration': 1000 * 60,
          'delay': 0
        }
      ].map(formatDeparture));
      assert.equal(q.toData().length, 3);
      q.push(formatDeparture({
        'id': 'a707f888a499bcdbf62703abbfbd69aaedc423ad',
        'name': 'in 12 min, 1min delay',
        'date': '2017-06-10T17:42:00.000Z',
        'walkingDuration': 1000 * 60,
        'delay': -60
      }));
      assert.equal(q.toData().length, 4);
      assert.equal(getIds(q.toData()).indexOf('a707f888a499bcdbf62703abbfbd69aaedc423ad') !== -1, true);
    });
  });
});
