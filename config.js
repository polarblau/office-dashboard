module.exports = {
  //
  timeDate: {

  },

  //
  bvg: {
    // accessId: 'FPLank1d39fe7d5f1bef03a9c2d37d77',
    // id: '009017104',
    address: 'Dunckerstraße 2a, 10437 Berlin',
    // address: 'Invalidenstrasse 116, 10115 Berlin',
    // address: 'Oraniendamm 11, 13469 Berlin'
    // address: 'Hermsdorfer Damm 192, Berlin'
  },

  weather: {
    appID: '9204ec314c16ddb58f56da811a13fb7b',
    q: 'Berlin, DE'
  },

  news: {
    apiKey: '28b4f791b3744c8897fa5cd59e00cd9a',
    sources: [
      { source: 'bloomberg', sortBy: 'top' },
      { source: 'cnn', sortBy: 'top' },
      { source: 'bbc-news', sortBy: 'top' },
      { source: 'the-guardian-uk', sortBy: 'latest' },
      { source: 'the-new-york-times', sortBy: 'top' }
    ]
  },

  //
  twitter: {

  },

  //
  slack: {

  }
};
