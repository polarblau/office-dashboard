const request = require('request');
const hash = require('object-hash');
const serializeParams = require('./../utils/serialize_params');

// ----------------------------------------------------------------------------

const ENDPOINT = 'http://demo.hafas.de/openapi/vbb-proxy/departureBoard';
const REQUEST_PARAMS   = {
  maxJourneys: 10,
  products: {
    suburban:  true,
    subway:    true,
    tram:      true,
    bus:       true,
    ferry:     false,
    express:   false,
    regional:  false
  },
  format:      'json',
  lang:        'en'
};

http://demo.hafas.de/openapi/vbb-proxy/LocationList?accessId=FPLank1d39fe7d5f1bef03a9c2d37d77&input=Dunckerstra%C3%9Fe%202A%2C%2010437%20Berlin&maxNo=3&type=S

// TODO:
// 1. Query nearest locations and store in memory
// 2. Determine walking distance to these stops and store in memory
// 3. Query schedule for top N nearest stops

const WALKING_DURATIONS = {
  'U1': 3 * 60 * 1000,
  'U7': 2 * 60 * 1000
};

// ----------------------------------------------------------------------------

class BVG {

  constructor(config) {
    if (!config.accessId) throw new Error('BVG: No accessId supplied.');
    this.config = config;
  }

  refresh(date) {
    if (!date) throw new Error('BVG: No request date supplied.');

    const params = serializeParams(Object.assign({}, REQUEST_PARAMS, this.config, {
      date: dateToRequestDate(date),
      time: dateToRequestTime(date)
    }));
    const url = ENDPOINT + '?' + params;

    return this.makeRequest(url)
      .then(data => data['Departure'].map(convertDepartureResponse));
  }

  makeRequest(url) {
    return new Promise((resolve, reject) => {
      request(url, (error, response, body) => {
        if (!error && response.statusCode == 200) {
          try {
            resolve(JSON.parse(body));
          } catch (e) {
            reject(Error(`BVG response could not be parsed. [${url}]\n${e.message}`));
          }
        } else {
          reject(Error(`BVG API could not be reached. [${url}]\n${body}`));
        }
      });
    });
  }
}

// ----------------------------------------------------------------------------

function dateToRequestDate(date) {
  return [
    date.getFullYear(),
    ('0' + (1 + date.getMonth())).slice(-2),
    ('0' + date.getDate()).slice(-2)
  ].join('-');
}

function dateToRequestTime(date) {
  return [
    ('0' + date.getHours()).slice(-2),
    ('0' + date.getMinutes()).slice(-2),
    ('0' + date.getSeconds()).slice(-2)
  ].join(':');
}

function convertDepartureResponse(data) {
  let line          = data.name.trim();
  let date          = new Date(Date.parse(data.date + ' ' + data.time));
  let direction     = data.direction.trim().replace(' (Berlin)', '');

  return {
    id:              hash(date + direction + line),
    line:            line,
    direction:       direction,
    date:            date,
    walkingDuration: WALKING_DURATIONS[line]
  };
}

// ----------------------------------------------------------------------------

module.exports = BVG;
