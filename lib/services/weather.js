const request = require('request');
const serializeParams = require('./../utils/serialize_params');

// ----------------------------------------------------------------------------

const ENDPOINT_TODAY = 'http://api.openweathermap.org/data/2.5/weather';
const ENDPOINT_WEEK   = 'http://api.openweathermap.org/data/2.5/forecast/daily';
const REQUEST_PARAMS   = { mode:  'json', units: 'metric' };

// ----------------------------------------------------------------------------

class WeatherService {

  constructor(config) {
    if (!config.appID) throw new Error('WeatherService: No appid supplied.');
    this.config = config;
  }

  refresh() {
    let buildParams = (params) => serializeParams(Object.assign({}, REQUEST_PARAMS, this.config, params));

    let todayUrl = ENDPOINT_TODAY + '?' + buildParams({ cnt: 1 });
    let getTodayWeather = this.makeRequest(todayUrl);

    let weekUrl = ENDPOINT_WEEK + '?' + buildParams({ cnt: 7 });
    let getWeekWeather = this.makeRequest(weekUrl);

    return Promise
      .all([getTodayWeather, getWeekWeather])
      .then(responses => {
        return {
          today: transformCurrentData(responses[0]),
          week: responses[1].list.slice(1).map(transformDailyData)
        };
      });
  }

  makeRequest(url) {
    return new Promise((resolve, reject) => {
      request(url, (error, response, body) => {
        if (!error && response.statusCode == 200) {
          resolve(JSON.parse(body));
        } else {
          reject(Error(`Weather API could not be reached. [${url}]\n${body}`));
        }
      });
    });
  }

}

// ----------------------------------------------------------------------------

function transformCurrentData(data) {
  let date = new Date(data.dt * 1000);

  return {
    id:            `${date.getDate()}${date.getMonth()+1}${date.getFullYear()}`,
    date:          date,
    sunrise:       new Date(data.sys.sunrise * 1000),
    sunset:        new Date(data.sys.sunset * 1000),
    temp:          data.main.temp,
    humidity:      data.main.humidity,
    pressure:      data.main.pressure,
    maxTemp:       data.main.temp_max,
    minTemp:       data.main.temp_min,
    windSpeed:     data.wind.speed,
    windDirection: data.wind.deg,
    description:   data.weather[0].description,
    icon:          data.weather[0].icon,
  };
}

function transformDailyData(data) {
  const date = new Date(data.dt * 1000);

  return {
    id:            `${date.getDate()}${date.getMonth()+1}${date.getFullYear()}`,
    date:          date,
    maxTemp:       data.temp.max,
    minTemp:       data.temp.min,
    icon:          data.weather[0].icon,
    weather:       data.weather[0].main,
    humidity:      data.humidity,
    windSpeed:     data.speed
  };
}

// ----------------------------------------------------------------------------

module.exports = WeatherService;
