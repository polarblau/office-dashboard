const request = require('request');
const hash = require('object-hash');
const serializeParams = require('./../utils/serialize_params');

// ----------------------------------------------------------------------------

const ENDPOINT = 'https://newsapi.org/v1/articles';

// ----------------------------------------------------------------------------

class News {

  constructor(config) {
    if (!config.apiKey) throw new Error('News: No apiKey supplied.');
    this.config = config;
  }

  refresh() {
    const params = { apiKey: this.config.apiKey };

    const sourceRequests = this.config.sources.map((source) => {
      let url = ENDPOINT + '?' + serializeParams(Object.assign({}, params, source));
      return this.makeRequest(url);
    });

    return Promise
      .all(sourceRequests)
      .then(responses => [].concat.apply([], responses).map(convertNewsResponse));
  }

  makeRequest(url) {
    return new Promise((resolve, reject) => {
      request(url, (error, response, body) => {
        if (!error && response.statusCode == 200) {
          let data = JSON.parse(body);
          if (data.status === 'ok') {
            resolve(data);
          } else {
            reject(Error(`newsapi.org rejected request. [${data.code}: ${data.message}]`));
          }
        } else {
          reject(Error(`newsapi.org could not be reached. [${url}]`));
        }
      });
    });
  }
}

// ----------------------------------------------------------------------------

function convertNewsResponse(data) {
  return data.articles.map((article) => {
    return {
      id:           hash(article),
      title:        article.title,
      date:         Date.parse(article.publishedAt),
      description:  article.description,
      source:       data.source
    }
  });
}

// ----------------------------------------------------------------------------

module.exports = News;
