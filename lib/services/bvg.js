const hash = require('object-hash');
const vbbClient = require('vbb-client');
const withCache = require('../utils/with_cache');
const googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyCzTKCoN9_jN_PKrZ6vMESdkWMl7iKtQJ0'
});

// ----------------------------------------------------------------------------

const MAX_STATIONS = 3;
const MAX_DISTANCE = 300;

// ----------------------------------------------------------------------------

class BVG {

  constructor(config) {
    this.config = Object.assign({}, {
      maxStations: MAX_STATIONS,
      maxDistance: MAX_DISTANCE
    }, config);
  }

  requestStations(address) {
    return findLocationForAddress(address)
      .then(findStationsNearLocation)
      .then(getWalkingDistances);
  }

  requestDepartures(stationID) {
    return queryDeparturesForStation(stationID);
  }

}

// ----------------------------------------------------------------------------

const formatLocation = (locations) => {
  return { address: locations[0].name, coordinates: locations[0].coordinates };
}

const findLocationForAddress = (query) => {
  if (!query) throw new Error('BVG#findLocationForAddress: missing query.');

  return vbbClient.locations(query, {
    stations: false,
    poi:      false,
    results:  1
  })
  .then(formatLocation);
};

// ---

const findStationsNearLocation = (location) => {
  return vbbClient.nearby({
    latitude:  location.coordinates.latitude, // *
    longitude: location.coordinates.longitude, // *
    results:   MAX_STATIONS, // seems to have no effect
    distance:  MAX_DISTANCE, // max in m
    stations:  true,
    poi:       false
  })
  .then(results => {
    return Object.assign(location, { stations: results.slice(0, MAX_STATIONS) });
  });
};

// ---

const getWalkingDistances = (location) => {
  const joinCoordinates = (coordinates, separator = ',') => {
    return [coordinates.latitude, coordinates.longitude].join(separator);
  };

  return new Promise((resolve, reject) => {
    googleMapsClient.distanceMatrix({
      origins:      [joinCoordinates(location.coordinates)],
      destinations: location.stations.map(station => joinCoordinates(station.coordinates)),
      units:        'metric',
      mode:         'walking',
    }, (err, response) => {
      if (!err) {
        const origin = response.json.rows[0];
        const stationsWithDistance = location.stations.map((station, i) => {
          return Object.assign({}, station, {
            duration: origin.elements[i].duration.value,
            distance: origin.elements[i].distance.value
          });
        });
        resolve(Object.assign({}, location, { stations: stationsWithDistance }));
      } else {
        reject(Error('[BVG#getWalkingDistances failed.' + err + response.json));
      }
    });
  });
};

// ---

const normalizeLineName = (line) => {
  let name = line.name;

  switch(line.product) {
    case 'tram':
      name = name.replace('Tram', '').trim();
      break;
    case 'bus':
      name = name.replace('Bus', '').trim();
      break;
  }

  return name;
}

const formatDeparture = (departure, stationID) => {
  return {
    id:        hash(stationID + (Date.parse(departure.when) + (+departure.delay * 1000)) + departure.direction + departure.line.name),
    direction: departure.direction,
    line:      normalizeLineName(departure.line),
    product:   departure.line.product,
    date:      departure.when,
    delay:    +departure.delay
  };
};

const queryDeparturesForStation = (stationID) => {
  return vbbClient.departures(stationID)
    .then(departures => departures.map(departure => formatDeparture(departure, stationID)))
    .catch(e => { console.error('Failed', stationID); throw e; });
};

// ----------------------------------------------------------------------------

module.exports = BVG;
