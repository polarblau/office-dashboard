const Cache = new Map();

// ----------------------------------------------------------------------------

const withCache = (key, execRequest) => {

  const log = function(msg, key) {
    console.log(`   > Cache: ${msg}` + (key ? ` "${key}"` : ''));
  }

  let result;

  log('request for', key);
  if (!Cache.has(key)) {
    log('not cached', key);
    result = execRequest().then(data => {
      log('writing', key);
      Cache.set(key, data);
      return data;
    }).catch(e => {
      log('error, removing', key);
      Cache.delete(key);
      throw e;
    });
  } else {
    log('cached, retrieving', key);
    try {
      result = Promise.resolve(Cache.get(key));
    } catch(e) {
      log('cached, error');
      console.error(e);
    }
  }

  return result;
};

// ----------------------------------------------------------------------------

module.exports = withCache;
