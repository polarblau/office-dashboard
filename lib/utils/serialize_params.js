function serializeParams(params) {
  let pairs = [];
  for (let key in params) {
    if (!params.hasOwnProperty(key)) continue;

    if (Object.prototype.toString.call(params[key]) === '[object Object]') {
      pairs.push(serializeParams(params[key]));
      continue;
    }

    pairs.push(key + '=' + params[key]);
  }
  return pairs.join('&');
}

module.exports = serializeParams;
